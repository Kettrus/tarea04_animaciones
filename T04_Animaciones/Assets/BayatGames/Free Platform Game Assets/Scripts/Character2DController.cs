using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character2DController : MonoBehaviour
{
    [SerializeField] public LayerMask platformLayerMask;
    public float MovementSpeed = 1;
    private Rigidbody2D _rigidbody;
    public BoxCollider2D boxCollider2D;
    public float vida;

    public Animator animator_character;
    public Animator chest;

    //Variables de suelo
    public LayerMask ground;
    public float distanciaRay;
    public float JumpForce;

    //Monedas
    public int Coin;
    public Text CoinContador;

    // Start is called before the first frame update
    void Start()
    {
        Coin = 0;
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Movimiento
        var movement = Input.GetAxis("Horizontal");
        if (movement <= -0.5f || movement >= 0.5f) {
            transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * MovementSpeed;
        }

        //Flip personaje
        if (Input.GetAxis("Horizontal") < 0) {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        if (Input.GetAxis("Horizontal") > 0) {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        //Animacion Correr
        if (movement <= -0.3f || movement >= 0.3f) {
            animator_character.SetBool("correr", true);
        } else {
            animator_character.SetBool("correr", false);
        }

        if (IsGrounded()) {
            animator_character.SetBool("salto", false);
        }

        //Salto
        if (Input.GetButtonDown("Jump") && Mathf.Abs(_rigidbody.velocity.y) < 0.001f && IsGrounded()) {
            _rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);

        }

        //Animacion Salto
        if (_rigidbody.velocity.y != 0) {
            animator_character.SetBool("salto", true);
        } else if (_rigidbody.velocity.y != 0) {
            animator_character.SetBool("salto", false);
        }

        //Contador
        CoinContador.text = Coin.ToString();

        if (Input.GetKeyDown(KeyCode.W)) {
            Debug.DrawRay(transform.position + Vector3.up, Vector2.right, Color.red, 2);
            RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up, Vector2.right);
            if ((hit.collider != null) && (hit.collider.CompareTag("Chest"))) {
                animator_character.SetBool("OPEN", true);
            }
        }

    }

    //Monedas
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Coin")) {
            Coin++;
            Destroy(collision.gameObject);
        }
    }

    bool IsGrounded() {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 0.5f;
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, ground);
        Debug.DrawRay(position, direction, Color.green, 1);

        if (hit.collider != null) {
            Debug.Log(hit.collider.name);
            return true;
        }
        return false;

    }

}
