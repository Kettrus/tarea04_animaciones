using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public Animator chest;
    public GameObject coin;
    public Collider2D collider;
    public int monedas;

    // Start is called before the first frame update
    void Start() {

    }

    private void OnTriggerEnter2D(Collider2D collision) {
      
            chest.SetBool("OPEN", true);
            for (int i = 0; i < monedas; i++) {
                Instantiate(coin, transform.position + new Vector3(0, 2, 0), Quaternion.identity);
            }
    }

    // Update is called once per frame
    void Update()
    {
       
            
    }

}
