using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemigos : MonoBehaviour
{

    public Text muerte;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.CompareTag("Player")) {
            muerte.enabled = true;
            Destroy(collision.gameObject);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
