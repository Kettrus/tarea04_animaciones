using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAttack : MonoBehaviour
{
    public Animator animator_enemigo;
    public Text muerte;

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.CompareTag("Player")) {
            muerte.enabled = true;
            Destroy(collision.gameObject);
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        animator_enemigo.SetTrigger("Ataque");
    }
}
